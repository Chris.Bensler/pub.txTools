@ECHO OFF

ECHO Converting DDS Textures to TGA...
FOR /R ..\textures %%I IN (*.dds) DO (
  readdxt "%%~dpI%%~nxI"
  MOVE "test.tga" "%%~dpI%%~nI.tga"
)
ECHO Done